import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";

// Create a VPC.
const vpc = new aws.ec2.Vpc("vpc", {
    cidrBlock: "10.0.0.0/16",
});

// Create a public subnet in the VPC.
const subnet = new aws.ec2.Subnet("subnet", {
    vpcId: vpc.id,
    cidrBlock: "10.0.1.0/24",
    mapPublicIpOnLaunch: true, // Enable public IP addresses.
});

// Create an Internet Gateway.
const igw = new aws.ec2.InternetGateway("igw", { vpcId: vpc.id });

// Create a route table.
const routeTable = new aws.ec2.RouteTable("routeTable", {
    vpcId: vpc.id,
    routes: [{ cidrBlock: "0.0.0.0/0", gatewayId: igw.id }],
});

// Associate the route table with the subnet.
const routeTableAssociation = new aws.ec2.RouteTableAssociation("routeTableAssociation", {
    routeTableId: routeTable.id,
    subnetId: subnet.id,
});

const ecsTaskExecutionRole = new aws.iam.Role("ecsTaskExecutionRole", {
    assumeRolePolicy: JSON.stringify({
        Version: "2012-10-17",
        Statement: [{
            Action: "sts:AssumeRole",
            Principal: {
                Service: "ecs-tasks.amazonaws.com"
            },
            Effect: "Allow",
            Sid: ""
        }]
    })
});

const ecsTaskExecutionRolePolicyAttachment = new aws.iam.PolicyAttachment("ecsTaskExecutionRolePolicyAttachment", {
    policyArn: "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
    roles: [ecsTaskExecutionRole.name]
});

export const ecsTaskExecutionRoleName = ecsTaskExecutionRole.name;


// Create a security group in the VPC that allows ingress on port 8080.
const securityGroup = new aws.ec2.SecurityGroup("securityGroup", {
    vpcId: vpc.id,
    ingress: [{
        protocol: "tcp",
        fromPort: 8080,
        toPort: 8080,
        cidrBlocks: ["0.0.0.0/0"],
    }],
    egress: [{
        protocol: "-1",
        fromPort: 0,
        toPort: 0,
        cidrBlocks: ["0.0.0.0/0"],
    }],
});


// Create an ECS cluster.
const cluster = new aws.ecs.Cluster("cluster");

// Define the Fargate Task with a simple nginx server
const fargateTaskDefinition = new aws.ecs.TaskDefinition("fargateTaskDefinition", {
    family: "fargate",
    cpu: "256",
    memory: "512",
    networkMode: "awsvpc",
    requiresCompatibilities: ["FARGATE"],
    executionRoleArn: ecsTaskExecutionRole.arn,
    containerDefinitions: JSON.stringify([{
        name: "fargate-container",
        image: "356552043242.dkr.ecr.eu-west-1.amazonaws.com/therapie-container-registry/bookingservice", // Nginx container image.
        cpu: 256,
        memory: 512,
        essential: true,
        portMappings: [{
            containerPort: 8080,
            hostPort: 8080,
            protocol: "tcp",
        }],
    }]),
},{dependsOn: [ecsTaskExecutionRolePolicyAttachment]});

// Define the Fargate Service.
const fargateService = new aws.ecs.Service("fargateService", {
    cluster: cluster.id,
    desiredCount: 1,
    launchType: "FARGATE",
    taskDefinition: fargateTaskDefinition.arn,
    networkConfiguration: {
        subnets: [subnet.id],
        securityGroups: [securityGroup.id],
        assignPublicIp: true,
    },
});
