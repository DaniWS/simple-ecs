# Web server on ECS

1. Clone this repository
2. Install and log into Pulumi: `pulumi login`
3. Install modules: `npm ci`
4. Initialize and name the stack: `pulumi stack init`
5. Load your AWS account credentials in Leapp
6. Deploy the stack to your AWS account: `pulumi up`
7. Get the public IP of the ECS task from the AWS ECS Console and find the service up at `https://PUBLIC-IP:8080`
